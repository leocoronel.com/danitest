import React, { useRef, useState } from "react"
import { StyleSheet, SafeAreaView, View, Button, TextInput } from "react-native"
import Constants from "expo-constants"
import { StatusBar } from "expo-status-bar"
import WebView from "react-native-webview"

export default function App() {
    const webViewRef = useRef<WebView>(null)
    const initialUri = `https://shoptimus-frontend-staging.bonpreu.staging.shoptimus.ai/entry/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyODM2MjMyIn0.qEq89mus_pdp2z46iF8UwL9boTSkdTHNNRP4YQvbyBc/es`
    const [inputData, setInputData] = useState(initialUri)
    const [currentURI, setURI] = useState(initialUri)

    return (
        <SafeAreaView style={[styles.screen]}>
            <View
                style={[styles.view]}
            >

                <WebView
                    ref={webViewRef}
                    source={{ uri: currentURI }}
                    style={styles.webview}
                    javaScriptEnabled
                    allowFileAccess
                    mixedContentMode="always"
                    domStorageEnabled
                    onShouldStartLoadWithRequest={(request) => {
                        if (request.url === currentURI) {
                            return true
                        }

                        setURI(request.url)
                        return false
                    }}
                />

                <View
                    style={[styles.overview]}
                >
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => setInputData(text.trim())}
                        value={inputData}
                    />

                    <Button
                        title="Go"
                        onPress={() => {
                            if (inputData.length === 0) {
                                setURI(initialUri)
                                return
                            }

                            setURI(inputData)
                        }}
                    />

                </View>

            </View>
            <StatusBar style="dark"
                       translucent={true}
                       hidden={false}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    screen: {
        paddingTop: Constants.statusBarHeight,
        flex: 1
    },
    view: {
        flex: 1
    },
    overview: {
        position: "absolute",
        backgroundColor: "white",
        zIndex: 1,
        bottom: 0,
        width: "100%"
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10
    },
    webview: {
        zIndex: 0,
        backgroundColor: "#f0f",
        flex: 1
    }
})